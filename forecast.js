//https://api.openweathermap.org/data/2.5/forecast?q=Mumbai&units=metric& id=524901&appid=3087a61bbb479b5ebe8f2efb83c4a50e

const key = "3087a61bbb479b5ebe8f2efb83c4a50e";
const getForecast = async (city) => {
    const base = "https://api.openweathermap.org/data/2.5/forecast"
    const query = `?q=${city}&units=metric&appid=${key}`;
    
    const response = await fetch(base+query);
    //console.log(response);
    
    if(!response.ok)
        throw new Error("Status code: " + response.status);
    
    const  data = await response.json();
    //console.log(data);
    return data;
}

getForecast('Mumbai')
.then(data=>console.log(data))
.catch(err=>console.log(err));