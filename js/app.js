(function($, document, window) {

    $(document).ready(function() {

        // Cloning main navigation for mobile menu
        $(".mobile-navigation").append($(".main-navigation .menu").clone());

        // Mobile menu toggle 
        $(".menu-toggle").click(function() {
            $(".mobile-navigation").slideToggle();
        });
    });

    $(window).load(function() {

    });

})(jQuery, document, window);


/********************COMMUNICATING WITH FORECAST JS*******************************/

const cityForm = document.querySelector('form');
const monthName = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep","Oct", "Nov", "Dec"];
const dayName = ["Sunday", "Monday", "Tueday", "Wednesday", "Thursday", "Friday", "Saturday"];

const updateUI = (data, cityName) =>{
    const days = document.getElementsByClassName('day');
    const date = document.querySelector('.date');
    const humidity = document.getElementById('humidity');
    const windSpeed = document.getElementById('wind-speed');
    const windDegree = document.getElementById('wind-degree');
    const location = document.querySelector('.location');
    const temps = document.getElementsByClassName('temp');
    const icons = document.getElementsByClassName('weather-icon');
    
    location.innerHTML = cityName;
    humidity.innerHTML = data.list[0].main.humidity + "%";
    
    windSpeed.innerHTML = (Math.round(data.list[0].wind.speed * 3.6 * 10)/10) + "km/hr";
    windDegree.innerHTML = data.list[0].wind.deg + "<sup>o</sup>C";
    
    const todaysDate = new Date(data.list[0].dt_txt);
    const todaysMonth = monthName[todaysDate.getMonth()];
    const todaysDay = todaysDate.getDate() + "" + todaysMonth;
    
    //cards kai liye for loop lagega kyuki jitne bhi cards badhte jahe toh for loop handle krr dehga
    var i = 0;
    var j = 0;
    for(let elements of days){
        const day = dayName[(todaysDate + i) % 7];
        elements.innerHTML = day;
        
        const temp = Math.round(data.list[j].main.temp*10)/10;
        temps[i].innerHTML = temp + "<sup>o</sup>C";
        
        icons.src = "images/icons/" + data.list[j].weather[0].icon +".svg";
        i++;
        j+=8;
    }
}

cityForm.addEventListener('submit', e=>{
    e.preventDefault();
    let cityName = cityForm.city.value.trim();
    if(cityName == ""){
        cityName = "Mumbai";
    }
    
    getForecast(cityName)
        .then(data => updateUI(data, cityName))
        .catch(err =>console.log(err));
});

//DEFAULT LOAD FOR MUMBAI
getForecast('Mumbai')
    .then(data => updateUI(data, 'Mumbai'))
    .catch(err => console.log(err));
